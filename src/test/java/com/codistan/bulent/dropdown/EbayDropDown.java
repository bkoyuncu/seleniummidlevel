package com.codistan.bulent.dropdown;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class EbayDropDown {

	public static void main(String[] args) {
		
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		driver.get("https://www.ebay.com");
		
		WebElement drpdwn =driver.findElement(By.xpath("//*[@id=\'gh-shop-a\']"));
		
		Select allDropDownMenu = new Select (drpdwn);
		
		allDropDownMenu.selectByVisibleText("scnd");
		
		
		
	}
	
	

}
