package com.codistan.bulent.dropdown;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class FBDropDown {

	public static void main(String[] args) {
		
		WebDriverManager.chromedriver().setup();
		
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://www.facebook.com/");
		driver.manage().window().maximize();		
		
		
		WebElement month_select=driver.findElement(By.xpath("//select[@id='month']"));
		Select sel=new Select(month_select);
		//sel.selectByValue("9");
		sel.selectByVisibleText("Sep");
	}

}
