package com.codistan.bulent.displayed;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class FBDisplayed {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.facebook.com");
		driver.manage().window().maximize();
		
		
		
		WebElement femaleRadioButton = driver.findElement(By.xpath("//input[@id='u_0_9']"));
		femaleRadioButton.click();
		boolean status = femaleRadioButton.isDisplayed();
		
		System.out.println("Button is displayed " + status);
	}

}
