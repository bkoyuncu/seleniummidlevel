package com.codistan.bulent.displayed;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class GmailDisplayed {

	public static void main(String[] args) {
		
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize(); 
		driver.get("http://www.gmail.com");
         
		 Boolean Display = driver.findElement(By.xpath("//*[@id='identifierNext']/span")).isDisplayed();
         
		 System.out.println("Element displayed is :"+Display);
	}

}
