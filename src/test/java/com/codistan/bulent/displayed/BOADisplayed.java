package com.codistan.bulent.displayed;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BOADisplayed {

	public static void main(String[] args) {
		
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.bankofamerica.com/");
		driver.manage().window().maximize();
		
		
		
		WebElement contactRadioButton = driver.findElement(By.xpath("//input[@id='saveOnlineId']"));
		contactRadioButton.click();
		boolean status = contactRadioButton.isDisplayed();
		
		System.out.println("Button displayed" + status);
			
		}


}
