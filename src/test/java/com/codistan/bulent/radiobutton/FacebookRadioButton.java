package com.codistan.bulent.radiobutton;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class FacebookRadioButton {
	
	public static void main(String[] args) {
		
	

	WebDriverManager.chromedriver().setup();
	WebDriver driver = new ChromeDriver();
	driver.get("https://www.facebook.com");
	driver.manage().window().maximize();
	
	
	
	WebElement customRadioButton = driver.findElement(By.xpath("//input[@id='u_0_b']"));
	customRadioButton.click();
	boolean status = customRadioButton.isSelected();
	
	System.out.println("Button is selected " + status);
}
}