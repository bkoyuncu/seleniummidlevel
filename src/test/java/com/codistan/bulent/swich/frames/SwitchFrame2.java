package com.codistan.bulent.swich.frames;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SwitchFrame2 {

	public static void main(String[] args) {
		
		WebDriverManager.firefoxdriver().setup();
		WebDriver driver = new FirefoxDriver();
		driver.get("https://www.softwaretestinghelp.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		 
		
		List<WebElement> elements = driver.findElements(By.tagName("iframe"));
		
		int numberOfTags = elements.size();
		
		System.out.println("No. of Iframes on this Web Page are: " +numberOfTags);
		
		System.out.println("Switching to the frame");
		
		driver.switchTo().frame("aswift_0");
		
		System.out.println("Frame Source" +driver.getPageSource());
		
		
		driver.switchTo().defaultContent();
		
		driver.quit();

	}

}
