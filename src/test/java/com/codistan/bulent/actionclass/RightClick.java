 package com.codistan.bulent.actionclass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class RightClick {

	public static void main(String[] args) {
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		 driver.get("https://demoqa.com/tooltip-and-double-click/");
		    System.out.println("demoqa webpage displayed");
		 		 
		 
		    Actions actions = new Actions(driver);
		 
		    WebElement btnElement = driver.findElement(By.id("rightClickBtn"));
		 
		    actions.contextClick(btnElement).perform();
		    System.out.println("Right click Context Menu displayed");
		 
		    WebElement elementOpen = driver.findElement(By.xpath(".//div[@id='rightclickItem']/div[1]"));  
		    elementOpen.click(); 
		 
		    driver.switchTo().alert().accept();
		    System.out.println("Right click Alert Accepted");
	}

}
