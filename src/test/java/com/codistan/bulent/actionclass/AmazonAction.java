package com.codistan.bulent.actionclass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class AmazonAction {

	public static void main(String[] args) {
		
		WebDriverManager.chromedriver().setup();
	      WebDriver driver = new ChromeDriver();    
	      driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	      driver.get("http://www.amazon.com");
	      driver.manage().window().maximize();

	      WebElement signIn = driver.findElement(By.xpath("//span[contains(text(),'Hello, Sign in')]"));
	      signIn.click();
	      WebElement createAccount = driver.findElement(By.xpath("//a[@id='createAccountSubmit']"));
	      createAccount.click();
	      
	      WebElement costumerName = driver.findElement(By.id("//input[@id='ap_customer_name']"));

	      
	      Actions act = new Actions(driver);
	      
	      Action action = act.moveToElement(costumerName).sendKeys(costumerName, "bulent").build();
	      			action.perform();
	}

}
