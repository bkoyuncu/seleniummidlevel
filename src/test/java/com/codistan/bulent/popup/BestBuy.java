package com.codistan.bulent.popup;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BestBuy {

	public static void main(String[] args) {
		
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();               		
        driver.get("https://www.bestbuy.com");			
        driver.manage().window().maximize();
        
       driver.findElement(By.xpath("button[@class='close']//span[contains(text(),'×')")).click();
        
        Alert alert = driver.switchTo().alert();
        
       alert.dismiss();
	}

}
