package com.codistan.bulent.popup;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DismissTinder {

	public static void main(String[] args) {
		
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://tinder.com");
		driver.manage().window().maximize();
		
		driver.findElement(By.xpath("//*[@class='D(b) Sq(20px)--ml Sq(16px)--s Fill(cc)']")).click();
		driver.findElement(By.cssSelector("#modal-manager > div > div > button > svg > path")).click();
		
	}

}
