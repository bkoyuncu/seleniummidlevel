package com.codistan.bulent.draganddrop;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DD1 {

		public static void main(String[] args) throws InterruptedException {
			
			WebDriverManager.chromedriver().setup();
			WebDriver driver = new ChromeDriver();
				driver.manage().window().maximize();
				driver.get("https://www.flipkart.com/search?q=laptop");
				WebElement leftSlider = driver.findElement(By.xpath("//div[@data-reactid='108']"));
				Actions builder = new Actions(driver);
				builder.dragAndDropBy(leftSlider, 60, 0)
						.build()
						.perform();
				Thread.sleep(5000);
				WebElement rightSlider = driver.findElement(By.xpath("//div[@data-reactid='110']"));
				builder.dragAndDropBy(rightSlider, -30, 0)
						.build()
						.perform();
				Thread.sleep(5000);
				WebElement source = driver.findElement(By.xpath("//img[@alt='Flipkart']"));
				WebElement target = driver.findElement(By.className("LM6RPg"));
				builder.dragAndDrop(source, target).build().perform();

	}


}