package com.codistan.bulent.mouseover;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TmobileMouseOver {

	public static void main(String[] args) {
		
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.t-mobile.com");
		driver.manage().window().maximize();
		
		

		Actions action = new Actions(driver);
		
		// WebElement elmt = driver.findElement(By.id("//*[@id=\'digital-header-nav-link-head-3\']"));
		WebElement coverage = driver.findElement(By.xpath("//a[@id='digital-header-nav-link-head-3']"));
		WebElement whatIs5G = driver.findElement(By.xpath("//span[contains(text(),'What is 5G')]"));
		
		action.moveToElement(coverage).moveToElement(whatIs5G).click().build().perform();
		
	
	}

}
