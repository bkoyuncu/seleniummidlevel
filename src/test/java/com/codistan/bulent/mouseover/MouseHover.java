package com.codistan.bulent.mouseover;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class MouseHover {

	public static void main(String[] args) {
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
	       driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	       driver.get("https://www.turkcell.com.tr");
	       
	       Actions action = new Actions(driver);
	       
	       WebElement element = driver.findElement(By.xpath("//nav[@class='m-header-nav']//a[contains(text(),'Cihazlar')]"));
	       element.click();
	       WebElement ele = driver.findElement(By.xpath("//body/header[@class='o-header o-header--top o-header--not-bottom']/div[@class='m-header-dropdown m-header-dropdown--scrolling']/div[@class='container']/div[@class='o-header__nav-container js-o-header-nav']/div[@class='o-header__sub-nav']/div[@class='m-grid']/div[@class='m-grid-col-6']/ul[@class='o-header__sub-nav-items']/li[8]/a[1]"));
	       action.moveToElement(ele).build().perform();
	}
}
