package com.codistan.bulent.mouseover;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class AmazonMouseHover {

	public static void main(String[] args) {

		
		WebDriverManager.chromedriver().setup();
		WebDriver driver =  new ChromeDriver();
        driver.get("https://www.amazon.com");
        WebElement newReleases = new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Today's Deals')")));
        new Actions(driver).moveToElement(newReleases).perform();
	}

}
