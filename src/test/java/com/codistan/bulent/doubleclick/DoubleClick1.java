package com.codistan.bulent.doubleclick;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DoubleClick1 {

	public static void main(String[] args) throws InterruptedException {
		
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://api.jquery.com/dblclick");
		driver.manage().window().maximize();
		  
		WebElement dbl = driver.findElement(By.cssSelector("html>body>div"));
	  
		 
		  Actions act = new Actions(driver);
		  act.doubleClick(dbl).build().perform();
		  Thread.sleep(3000);
		  driver.close();
		  
	}

}
