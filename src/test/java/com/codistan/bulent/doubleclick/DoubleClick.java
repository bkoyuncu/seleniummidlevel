package com.codistan.bulent.doubleclick;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DoubleClick {
	
	public static void main(String[] args) throws Exception{
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://www.woot.com/");
		driver.manage().window().maximize();
		//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.xpath("/html/body/div[6]/div/div/div[2]/div[2]")).click();
		//ctrl+shift+o
		//Properties 
		
		Actions actContainer = new Actions(driver);
		driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[2]/p")).click();
		WebElement homeKitchen=driver.findElement(By.xpath("//span[contains(text(),'Home & Kitchen')]"));
		//WebElement Spoil=driver.findElement(By.cssSelector("#category-tab-home-woot > section > div.wootplus-events > ul > li:nth-child(2) > a"));
		
		
		
		actContainer.moveToElement(homeKitchen).perform();
		Thread.sleep(3000);
		actContainer.doubleClick(driver.findElement(By.xpath("//a[contains(text(),'Spoil Yourself, Friend')]"))).perform();

	}

}
